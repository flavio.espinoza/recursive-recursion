import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import './App.css'
import RecursiveComponent from './components/RecursiveComponent'
import AddTodoButton from './components/AddTodoButton'
import NewTodoInput from './components/NewTodoInput'
import { addTodo, deletePost, fetchTodos } from './actions'
import { ReactReduxContext, useDispatch } from 'react-redux'

// @todo : change testData to api call
const testData = {
  user_id: '000001',
  email: 'flavio.espinoza@gmail.com',
  todo: [
    {
      name: 'Build a House',
      id: 'build_a_house',
      items: [
        {
          name: 'Design and Documentation',
          id: 'design_and_documentation',
          items: [
            {
              name: 'Drawings',
              id: 'level31',
              path: '#',
              items: [
                {
                  name: 'Sketches',
                  id: 'sketches',
                  path: '#',
                },
                {
                  name: 'Rendering',
                  id: 'rendering',
                  path: '#',
                  items: [
                    {
                      name: '3D Rendering',
                      id: '3d_rendering',
                      path: '#',
                    },
                    {
                      name: 'Site Plan Rendering',
                      id: 'site_plan_rendering',
                      path: '#',
                    },
                  ],
                },
              ],
            },
            {
              name: 'Construction Documents',
              id: 'construction_documents',
              path: '#',
            },
          ],
        },
        {
          name: 'Permitting',
          id: 'permitting',
          path: '#',
        },
        {
          name: 'Construction',
          id: 'construction',
          items: [
            {
              name: 'Buy Materials',
              id: 'buy_materials',
              path: '#',
            },
            {
              name: 'Hire Mason',
              id: 'hire_mason',
              path: '#',
            },
            {
              name: 'Hire Framer',
              id: 'hire_framer',
              path: '#',
            },
          ],
          path: '#',
        },
      ],
    },
    {
      name: 'Build a Deck',
      id: 'build_a_deck',
      items: [
        {
          name: 'Design and Documentation',
          id: 'design_and_documentation',
          items: [
            {
              name: 'Drawings',
              id: 'level31',
              path: '#',
              items: [
                {
                  name: 'Sketches',
                  id: 'sketches',
                  path: '#',
                },
                {
                  name: 'Rendering',
                  id: 'rendering',
                  path: '#',
                  items: [
                    {
                      name: '3D Rendering',
                      id: '3d_rendering',
                      path: '#',
                    },
                    {
                      name: 'Site Plan Rendering',
                      id: 'site_plan_rendering',
                      path: '#',
                    },
                  ],
                },
              ],
            },
            {
              name: 'Construction Documents',
              id: 'construction_documents',
              path: '#',
            },
          ],
        },
        {
          name: 'Permitting',
          id: 'permitting',
          path: '#',
        },
        {
          name: 'Construction',
          id: 'construction',
          items: [
            {
              name: 'Buy Materials',
              id: 'buy_materials',
              path: '#',
            },
            {
              name: 'Hire Mason',
              id: 'hire_mason',
              path: '#',
            },
            {
              name: 'Hire Framer',
              id: 'hire_framer',
              path: '#',
            },
          ],
          path: '#',
        },
      ],
    },
  ],
}

function App(props) {
  const { store } = React.useContext(ReactReduxContext)

  const [userData, setUserData] = React.useState({})
  const [todoList, setTodoList] = React.useState([])
  const [openLevels, setOpenLevels] = React.useState([])
  const [newTodoText, setNewTodoText] = React.useState('')

  React.useEffect(() => {
    // API call for the user's data
    props.fetchTodos()
  }, [])

  React.useEffect(() => {
    const state = store.getState()
    setUserData(testData)
    setTodoList(testData.todo)
  }, [userData, todoList])

  const toggleLevel = (level) => {
    if (level === null) {
      setOpenLevels([])
    } else if (openLevels.includes(level)) {
      setOpenLevels(openLevels.filter((item) => item !== level))
    } else {
      setOpenLevels([...openLevels, level])
    }
  }

  const handleClick = (id) => {
    setOpenLevels([id])
  }

  const handleTodoTextChange = (text) => {
    setNewTodoText(text)
  }

  const handleSubmit = () => {
    // dispatch({ type: 'ADD_TODO', payload: newTodoText })
  }

  return (
    <div className={'App p-4'}>
      <div className={'pb-8'}>
        <h1>Recursive Todo List</h1>
      </div>

      <ul className={'mb-2'}>
        {todoList.map((obj) => (
          <li className={'p-4 mb-2 shadow-md bg-white'} key={obj.id}>
            {openLevels.length > 0 ? (
              <RecursiveComponent
                openLevels={openLevels}
                toggleLevel={toggleLevel}
                {...obj}
              />
            ) : (
              <div
                className={'flex flex-row justify-between'}
                key={`${obj.id}_list_item`}
              >
                <a className={'p-2'} onClick={() => handleClick(obj.id)}>
                  {obj.name}
                </a>
                <a className={'p-2'} onClick={() => props.deletePost(obj.id)}>
                  Delete
                </a>
              </div>
            )}
          </li>
        ))}
      </ul>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {...state}
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ addTodo, deletePost, fetchTodos }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
