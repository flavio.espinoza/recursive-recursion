import React from "react";
import cn from "classnames";

function RecursiveComponent(props) {
  const { openLevels, toggleLevel, name, id, items } = props;
  const hasChildren = (children) => {
    return children && children.length;
  };
  const isOpen = openLevels.includes(id);
  const isActive = openLevels[openLevels.length - 1] === id;
  return (
    <ul
      className={cn(
        "stack-navigation",
        isOpen && "is-open",
        isActive && "is-active"
      )}
    >
      <li className="heading">
        <button onClick={() => toggleLevel(id)}>Back</button>
        <span>{name}</span>
      </li>
      {hasChildren(items) &&
        items.map((item) => (
          <li key={item.id}>
            {hasChildren(item.items) ? (
              <button type="button" onClick={() => toggleLevel(item.id)}>
                {item.name} (
                {`${item.items.length} ${
                  item.items.length === 1 ? "task" : "tasks"
                }`}
                )
              </button>
            ) : (
              <a href={item.path}>{item.name}</a>
            )}
            {hasChildren(item.items) && (
              <RecursiveComponent
                openLevels={openLevels}
                toggleLevel={toggleLevel}
                key={item.id}
                {...item}
              />
            )}
          </li>
        ))}
    </ul>
  );
}

export default RecursiveComponent;
