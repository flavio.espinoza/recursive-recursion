import React from 'react'

function AddTodoButton({ onClick }) {
  return (
    <div className={'mb-4'}>
      <button className={'btn-blue'} onClick={onClick}>+</button>
    </div>
  )
}

export default AddTodoButton