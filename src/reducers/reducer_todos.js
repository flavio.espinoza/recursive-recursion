export default function reducer_todos(state = [], action) {
  switch(action.type) {
    case 'FETCH_TODOS_SUCCESS':
      return action.todos.data;
    default:
      return state 
  }
}